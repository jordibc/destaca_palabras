"""
Test functions in iwords.py. To run with pytest.
"""

import os
from tempfile import NamedTemporaryFile

import iwords


def test_get_freqs():
    with NamedTemporaryFile() as fp:
        fp.write(b'Hello world!')
        fp.flush()
        assert iwords.get_freqs([fp.name]) == {'hello': 0.5, 'world': 0.5}


def test_clean():
    assert iwords.clean('Cómo estás') == 'como estas'


def test_run():
    assert os.system('./iwords.py --help') == 0

    with NamedTemporaryFile() as fp:
        fp.write(b'Hello world!')
        fp.flush()

        assert os.system(f'./iwords.py {fp.name}') == 0
        assert os.system(f'./iwords.py {fp.name} --show-score') == 0
        assert os.system(f'./iwords.py {fp.name} --limit 1') == 0
        assert os.system(f'./iwords.py {fp.name} --learn /usr/share/dict/words') == 0
