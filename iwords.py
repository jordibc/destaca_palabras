#!/usr/bin/env python

"""
Find interesting words in a group of text files.
"""

import re
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt


def main():
    args = get_args()

    freqs = get_freqs(args.files)

    priors = get_freqs(args.learn_from)

    min_priors = min(priors.values()) if priors else 1

    def score(word):
        # Likelihood ratio. And take into account Zipf's law for unknown priors.
        return freqs[word] / priors.get(word, min_priors / len(word))

    for i, word in enumerate(sorted(freqs.keys(), key=score, reverse=True)):
        score_txt = ('%-12g' % score(word)) if args.show_score else ''
        print(score_txt + word)
        if args.limit != 0 and i >= args.limit:
            break


def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)
    add = parser.add_argument  # shortcut
    add('files', metavar='FILE', nargs='+', default=[],
        help='text files where we look for interesting words')
    add('--learn-from', metavar='FILE', nargs='+', default=[],
        help='text files used to learn the normal frequency of words')
    add('--show-score', action='store_true', help='show the score of each word')
    add('--limit', type=int, default=10,
        help='number of output words (0 for no limit)')
    return parser.parse_args()


def get_freqs(files):
    "Return a dictionary of {word: frequency}"
    freqs = {}
    nwords = 0
    for fname in files:
        try:
            text = clean(open(fname).read())
            for word in re.findall(r'\b[a-z][a-z0-9]{2,}\b', text):
                freqs.setdefault(word, 0)
                freqs[word] += 1
                nwords += 1
        except IOError as e:
            print('Problem with "%s" (we ignore this file): %s' % (fname, e))

    for word in freqs:
        freqs[word] /= nwords
    # NOTE: we can instead estimate the frequency with "additive smoothing", but
    # probably not worth it: https://en.wikipedia.org/wiki/Additive_smoothing

    return freqs


def clean(text):
    "Return text without diacritics and in lowercase"
    chars_ugly = 'ÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÑñÇçABCDEFGHIJKLMNOPQRSTUVWXYZ'
    chars_nice = 'aeiouaeiouaeiouaeiouaeiouaeiounnccabcdefghijklmnopqrstuvwxyz'
    tr = {ord(ugly): ord(nice) for ugly,nice in zip(chars_ugly, chars_nice)}
    return text.translate(tr)



if __name__ == '__main__':
    main()
